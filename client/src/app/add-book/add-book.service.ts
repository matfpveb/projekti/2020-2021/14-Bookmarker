import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BookComponent } from '../book/book.component';
import { Book } from '../models/book.model';

@Injectable({
  providedIn: 'root'
})
export class AddBookService {

  public registerComponent: BookComponent;

  private readonly addBookUrl = 'http://localhost:3000/addBook/';

  constructor(private http: HttpClient) { }

  public createBook(data: any): Observable<Book>{
    const body = {...data};
    return this.http.post<Book>(this.addBookUrl, body);
  }

  public createImage(formData : any, data : any): Observable<any>{
    const body = {...data};
    return this.http.post<any>(this.addBookUrl, formData, body);
  }

  public getAllBooks(){
    return this.http.get(this.addBookUrl);
  }
}
