export type UserRole = "admin"|"user";

export interface UserI{
    _id: string,
    name: string,
    surname: string,
    email: string,
    username: string,
    image: string,
    following: string[],
    followers: string[],
    age: number,
    place: string,
    role: UserRole
}

export class User{
    public constructor(
    public id: string,
    public name: string,
    public surname: string,
    public email: string,
    public username: string,
    public image: string,
    public following: [string],
    public followers: [string],
    public age: number,
    public place: string,
    public role?: UserRole
    ) {}
}