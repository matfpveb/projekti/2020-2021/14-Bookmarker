import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SimpleSearchService {
  configUrl = "http://localhost:3000/search/"
  constructor(private http: HttpClient) { 

  }
  
  private handleError(error: HttpErrorResponse){
    if(error.status===0){
      //Todo
      console.log("client side or network error", error);
    }
    else{
      console.log("backend error:", error);
    }
    return throwError("Try again later :(");
  }

  runSearch(search: string) {
    return this.http.post<Array<Object>>(this.configUrl, {"simpleSearch": {"keywords" :search}}).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

}
