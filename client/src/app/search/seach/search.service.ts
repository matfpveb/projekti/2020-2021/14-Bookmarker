import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { SearchI } from 'src/app/models/search.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  configUrl = "http://localhost:3000/search/"

  constructor(private http: HttpClient) { 
    
  }

  private handleError(error: HttpErrorResponse){
    if(error.status===0){
      //Todo
      console.log("client side or network error", error);
    }
    else{
      console.log("backend error:", error);
    }
    return throwError("Try again later :(");
  }


  runSearch(search: SearchI) {
    // TODO: navesti tip koji vraca tako da bude lakse za rukovanje, ili makar odrediti posle sa as
    return this.http.post<Array<Object>>(this.configUrl, search).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

}
