import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FilterField } from '../filter-field/filter-field.component';
@Component({
  selector: 'app-filter-fields',
  templateUrl: './filter-fields.component.html',
  styleUrls: ['./filter-fields.component.css']
})
export class FilterFieldsComponent implements OnInit {

  @Input() fields: FilterField[];
  @Output() onFieldsChange = new EventEmitter<FilterField[]>();
  
  constructor() { }

  ngOnInit(): void {
  }

  onFieldChanged(event: FilterField): void{
    this.fields.find((field: FilterField)=>field.fieldName===event.fieldName).currentValue = event.currentValue;   
    this.onFieldsChange.emit(this.fields); 
  }

}
