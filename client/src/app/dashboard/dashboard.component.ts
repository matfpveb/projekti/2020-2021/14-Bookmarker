import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { StoriesService } from '../stories/stories.service';
import { ReviewService } from '../services/review.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ProbaComponent } from '../proba/proba.component';
import { DataService } from '../services/data.service';
import { EditProfileFormularComponent } from '../edit-profile-formular/edit-profile-formular.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  content: string;
  stories: any = [];
  reviews: any = [];
  idUser: string = null;
  profile: boolean = false;
  private sub: Subscription;
  public loggedInUser: User = null;
  user: User = null;
  num: any;
  buttonType: string = null;
  followers: any = [];
  following: any = [];

  constructor(private userService: UserService,
    private storiesService: StoriesService,
    private authService: AuthService,
    private reviewService: ReviewService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private data: DataService) {

    this.sub = this.authService.user.subscribe((user: User) => {
      this.loggedInUser = user;
    });

    this.authService.sendUserDataIfExists();

    this.num = 2;
  }

  ngOnInit(): void {

    this.content = 'stories';

    this.route.paramMap.subscribe(params => {
      this.idUser = params.get('idUser');
      // this.idUser = "607c7371f049e737a8b166cb";
    });

    if (this.idUser === this.loggedInUser.id) {
      this.profile = true;
    }

    this.storiesService.getStories(this.idUser).subscribe(stories => {
      this.stories = stories;
      console.log(stories)
    });
    this.reviewService.getUserReviews(this.idUser).subscribe(reviews => {
      this.reviews = reviews;
      // console.log(reviews);
    });
    this.userService.getUserInfo(this.idUser).subscribe((user: any) => {
      this.user = user;
      for(let index in user.following){
        this.userService.getUserInfo(user.following[index]).subscribe((user: any) => {
          this.following.push(user);
        });
      }
      for(let index in user.followers){
        this.userService.getUserInfo(user.followers[index]).subscribe((user: any) => {
          this.followers.push(user);
        });
      }
    });

    this.getTypeOfButton();

    console.log(this.loggedInUser);

  }

  public openFollowersDialog() {
    this.data.changeUsers(this.followers);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    this.dialog.open(ProbaComponent, dialogConfig);
  }

  public openFollowingDialog() {
    this.data.changeUsers(this.following);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = false;
    this.dialog.open(ProbaComponent, dialogConfig);
  }

  public changeContent() {
    if (this.content === 'stories')
      this.content = 'reviews';
    else
      this.content = 'stories';
  }

  public editProfile() {
    this.dialog.open(EditProfileFormularComponent);
  }

  public follow() {
    this.userService.follow(this.loggedInUser.id, this.idUser).subscribe((user: User) => {
      console.log("Zapracen korisnik " + this.idUser);
      this.getTypeOfButton();
      // window.location.reload();
    });

  }

  public unfollow() {
    this.userService.unfollow(this.loggedInUser.id, this.idUser).subscribe((user: User) => {
      console.log("Otpracen korisnik " + this.idUser);
      this.getTypeOfButton();
      // window.location.reload();
    });
  }

  formatDate(date: Date) {
    var date2 = new Date(date)
    var dd = String(date2.getDate()).padStart(2, '0');
    var mm = String(date2.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = date2.getFullYear();

    return dd + '.' + mm + '.' + yyyy;
  }

  public findPath(path: String) {
    return "http://localhost:3000/" + path;
  }

  public getTypeOfButton() {
    if (this.profile)
      this.buttonType = 'Edit profile';
    else if (this.isFollowing())
      this.buttonType = 'Followed';
    else if (this.isNotFollowing())
      this.buttonType = 'Follow';
    else
      this.buttonType = 'Nothing';
  }

  public isFollowing(): boolean {
    if (!this.profile && this.loggedInUser && this.loggedInUser.following.includes(this.idUser))
      return true;
    else
      return false;
  }

  public isNotFollowing(): boolean {
    if (!this.profile && this.loggedInUser && !this.loggedInUser.following.includes(this.idUser))
      return true;
    else
      return false;
  }

  public storiesCounter(): number {
    if (this.user)
      return this.stories.length;
    else
      return 0;
  }


  public reviewsCounter(): number {
    if (this.user)
      return this.reviews.length;
    else
      return 0;
  }

  public followingCounter() {
    if (this.user !== undefined)
      return this.user?.following.length;
    else
      return 0;
  }

  public followersCounter() {
    if (this.user !== undefined)
      return this.user?.followers.length;
    else
      return 0;
  }

  public addStory() {
    this.num = this.num + 2;
    if (this.num > this.stories.length)
      this.num = this.stories.length;
    console.log(this.user)
  }

  public closeStories() {
    this.num = this.num - 2;
    if (this.num === 0)
      this.num = 1;

  }

  truncate(str: string, len: number) {
    if (str.length > len && str.length > 0) {
      let newStr = str + ' '
      newStr = str.substr(0, len)
      newStr = str.substr(0, newStr.lastIndexOf(' '))
      newStr = newStr.length > 0 ? newStr : str.substr(0, len)
      return newStr + '...'
    }
    return str
  }

  public storyIsLiked(story: any): boolean {
    const isLiked = story.usersThatLikeStory.includes(this.loggedInUser.id);
    return isLiked;
  }

  public howManyLikes(story:any):Number{
    return story.usersThatLikeStory.length
  }

  public likeStory(storyId: string) {

      this.storiesService.likeStory(this.loggedInUser.id, storyId).subscribe(story => {
        console.log("lajkovano");
      });
    }

  public dislikeStory(storyId: string) {
    
    this.storiesService.dislikeStory(this.loggedInUser.id, storyId).subscribe(story => {
      console.log("dislajkovano");
    });
  }
}
