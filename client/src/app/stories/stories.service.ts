import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Story } from '../models/story.model';

@Injectable({
  providedIn: 'root'
})
export class StoriesService {

  private readonly storiesUrl = 'http://localhost:3000/stories/';
  private readonly storiesAddUrl = 'http://localhost:3000/stories/add';

  constructor(private http: HttpClient) { }

  getFollowingStories(idUser: any){
    return this.http.get(this.storiesUrl + 'following/' + idUser);
  }

  getStories(idUser: any): Observable<Story>{
    return this.http.get<Story>(this.storiesUrl + idUser);
  }

  public likeStory(userId:string,storyId:string) {
    const body = {
      userId : userId,
      storyId : storyId
    }
    return this.http.patch(this.storiesUrl + 'like' , body);
  }

  public dislikeStory(userId:string,storyId:string) {
    const body = {
      userId : userId,
      storyId : storyId
    }
    return this.http.patch(this.storiesUrl + 'dislike' , body);
  }
  
  public postStory(data: any){
    const body = {...data}
    return this.http.post<any>(this.storiesAddUrl, body)
  }

}
