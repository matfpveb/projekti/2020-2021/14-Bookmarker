import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';
import { ReviewService } from '../services/review.service';
import { StoriesService } from './stories.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddStoryComponent } from '../add-story/add-story.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.css']
})
export class StoriesComponent implements OnInit {

  stories: any = [];
  reviews: any = [];
  idUser: any;
  private sub: Subscription;
  public loggedInUser: User = null;
  content: string;

  public storyForm: FormGroup;
  buttonType: string = null;

  public num: any = 4;

  constructor(private storiesService: StoriesService,
    private reviewService: ReviewService,
    private router: Router,
    private authService: AuthService,
    private dialog: MatDialog) {

    this.sub = this.authService.user.subscribe((user: User) => {
      this.loggedInUser = user;
    });

    this.authService.sendUserDataIfExists();
  }

  ngOnInit(): void {

    this.content = 'stories';

    if (!this.loggedInUser) {
      this.router.navigate(['\login']);
    }

    this.storiesService.getFollowingStories(this.loggedInUser.id).subscribe((stories: any) => {
      this.stories = stories;
    });
    this.reviewService.getFollowingReviews(this.loggedInUser.id).subscribe(reviews => {
      this.reviews = reviews;
    });

  }

  public changeContent() {
    if (this.content === 'stories')
      this.content = 'reviews';
    else
      this.content = 'stories';
  }

  public submitStory(data: any){
    data['storyAuthor'] = this.loggedInUser.id;

    this.storiesService.postStory(data).subscribe((story: any) => {
      window.location.reload();
      console.log(story);
    });
  }

  public openAddStory(){
    this.dialog.open(AddStoryComponent);
  }

  public addStories(){
    if (this.num < this.reviews.length)
      this.num = this.num + 2;
  }

  public closeStories(){
    this.num = 4;
  }

  public truncate(str: string, len: number) {
    if (str.length > len && str.length > 0) {
      let newStr = str + ' '
      newStr = str.substr(0, len)
      newStr = str.substr(0, newStr.lastIndexOf(' '))
      newStr = newStr.length > 0 ? newStr : str.substr(0, len)
      return newStr + '...'
    }
    return str
  }

  public stripTags(input: string) {
    return input.replace(/<(?:.|\n)*?>/gm, '')
  }

  public storyIsLiked(story: any): boolean {
    const isLiked = story.usersThatLikeStory?.includes(this.loggedInUser.id);
    return isLiked;
  }

  public howManyLikes(story:any):Number{
    return story.usersThatLikeStory.length;
  }

  public likeStory(storyId: string) {

      this.storiesService.likeStory(this.loggedInUser.id, storyId).subscribe(story => {
        console.log("lajkovano");
      });
    }

  public dislikeStory(storyId: string) {
    
    this.storiesService.dislikeStory(this.loggedInUser.id, storyId).subscribe(story => {
      console.log("dislajkovano");
    });
  }
}