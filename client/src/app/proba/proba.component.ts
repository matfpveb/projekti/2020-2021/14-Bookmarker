import { Component, OnInit } from '@angular/core';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-proba',
  templateUrl: './proba.component.html',
  styleUrls: ['./proba.component.css']
})
export class ProbaComponent implements OnInit {

  users: any;

  constructor(private data: DataService) { }

  ngOnInit(): void {

    this.data.users.subscribe(users => this.users = users);

  }

  public findPath(path: String) {
    return "http://localhost:3000/" + path;
  }

}
