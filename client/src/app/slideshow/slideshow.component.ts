import { Component, OnInit } from '@angular/core';
import { delay, timeout } from 'rxjs/operators';
import { SlideshowService } from './slideshow.service';

declare const M: any;

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.css']
})
export class SlideshowComponent implements OnInit {

  tmpArr: any;
  books: any;

  topPicks: any = [];
  bestAuthors: any = [];
  mostReviewed: any = [];

  allBooks: any;
  allReviews: any;
  allAuthors: any;

  options = {
    fullWidth: true,
    indicators: true,
    duration: 300
  }

  constructor(private slideshowService: SlideshowService) { }

  ngOnInit() {
    this.getRandomBooks();
    this.getBooks();
  }

  initCarousel() {
    var collapsible = document.querySelectorAll('.collapsible');
    var CollapsibleInstances = M.Collapsible.init(collapsible);
    var carousel = document.querySelector('.carousel');
    var CarouselInstances = M.Carousel.init(carousel, {
      fullWidth: true,
      indicators: true,
      duration: 300
    });
    setTimeout(autoplay, 5500);
    function autoplay() {
      CarouselInstances.next();
      setTimeout(autoplay, 5500);
    }
  }

  public getBooks() {
    this.slideshowService.getBooks().subscribe((data: any) => {
      this.allBooks = data;
      this.getReviews();
    });
  }

  public getReviews() {
    this.slideshowService.getReviews().subscribe((data: any) => {
      this.allReviews = data;
      this.getAuthors();
    });
  }

  public getAuthors() {
    this.slideshowService.getAuthors().subscribe((data: any) => {
      this.allAuthors = data;
      this.getTopPicks();
    });
  }

  public getRandomBooks() {
    this.slideshowService.getBooks().subscribe((data: any) => {
      this.tmpArr = data;
      this.tmpArr = this.tmpArr.sort(() => Math.random() - 0.5)
      this.books = [];
      let size = this.tmpArr.length < 5 ? this.tmpArr.length : 5;
      for (var i = 0; i < size; i += 1) {
        this.books.push(this.tmpArr[i])
      }
      setTimeout(this.initCarousel, 500);
    });
  }

  public findPath(path: String) {
    return "http://localhost:3000/" + path.replace('\\', '/');
  }

  public getTopPicks() {
    for (var i = 0; i < this.allBooks.length; i++) {
      var num = 0;
      var sum = 0;
      for (var j = 0; j < this.allReviews.length; j++) {
        if (this.allReviews[j].book == this.allBooks[i]._id && this.allReviews[j].reviews.rating > 0) {
          num++;
          sum += this.allReviews[j].reviews.rating;
        }
      }
      if (num > 0)
        this.allBooks[i].avg = sum / num;
      else
        this.allBooks[i].avg = 0;
    }

    this.allBooks.sort(function (b1: any, b2: any) {
      var t1 = b1.avg;
      var t2 = b2.avg;
      if (t1 < t2)
        return 1;
      else
        return -1
    });

    let numTP = 4;
    if(this.allBooks.length < 4)
      numTP = this.allBooks.length;

    for (var i = 0; i < numTP; i++) {
      this.topPicks[i] = this.allBooks[i];
    }

    this.getBestAuthors();
  }

  public getBestAuthors() {
    for (var i = 0; i < this.allAuthors.length; i++) {
      var sum = 0;
      var num = 0;
      for (var j = 0; j < this.allBooks.length; j++) {
        
        if (this.allAuthors[i]._id == this.allBooks[j].author._id) {
          num++;
          sum += this.allBooks[j].avg;
        }
      }

      if (num > 0)
        this.allAuthors[i].avg = sum / num;
      else
        this.allAuthors[i].avg = 0;
    }

    this.allAuthors.sort(function (a1: any, a2: any) {
      var t1 = a1.avg;
      var t2 = a2.avg;
      if (t1 < t2)
        return 1;
      else
        return -1
    });

    let numAuthors = 4;
    if(this.allAuthors.length < 4)
      numAuthors = this.allAuthors.length;
    
    for(var i = 0; i < numAuthors; i++){
      this.bestAuthors[i] = this.allAuthors[i]; 
    }

    this.getMostReviewed();
  }

  public getMostReviewed() {
    for (var i = 0; i < this.allBooks.length; i++) {
      var sum = 0;
      for (var j = 0; j < this.allReviews.length; j++) {
        if (this.allBooks[i]._id == this.allReviews[j].book) {
          sum++;
        }
      }

      this.allBooks[i].numReviews = sum;
    }

    this.allBooks.sort(function (b1: any, b2: any) {
      var t1 = b1.numReviews;
      var t2 = b2.numReviews;
      if (t1 < t2)
        return 1;
      else
        return -1
    });

    let numReviewed = 4;
    if(this.allBooks.length < 4)
      numReviewed = this.allBooks.length;

    for(var i = 0; i < numReviewed; i++){
      this.mostReviewed[i] = this.allBooks[i];
    }
  }

}



