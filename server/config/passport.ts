import { ObjectId } from 'mongodb';
import Strategy from 'passport-google-oauth20';
const User = require('../src/models/user');
import { IUserDocument } from '../src/models/user';

export default function initPassport(passport: any) { // TODO: koji je tacno tip?
    passport.use(new Strategy.Strategy({
        clientID : '908008554010-ng4du5avsisc8r9d03nbm4j4u1oarb2m.apps.googleusercontent.com',
        clientSecret: 'l3gqjJIHzeI-IuBfIEM6MpJW',
        callbackURL: '/auth/google/callback'
    }, 
    async (accessToken, refreshToken, profile, done) => {
        const newUser = {
            googleId: profile.id,
            name: profile.name!.givenName,
            surname: profile.name!.familyName,
            image: profile.photos && profile.photos.length !== 0 ? profile.photos[0] : "",
            createdAt: new Date(),
            email: profile.emails && profile.emails.length !== 0 ? profile.emails[0] : "",
            password: "",
            username: profile.username,
        } as IUserDocument;
        try{
            let user = await User.findOne({ googleId: profile.id })

            if(user){
                //user already exists
                done(null, user)
            }
            else{
                //creating new user
                user = await User.create(newUser)
                done(null, user)
            }
        }
        catch(err){
            console.error(err);
        }
    }))

    passport.serializeUser((user: IUserDocument, done: Function) => {
        done(null, user.googleId); // TODO: vidi da li je ovde googleId ili id? da li je UserI ili neki drugi tip
    });

    passport.deserializeUser((id: ObjectId, done: Function) => {
        User.findById(id, (err: any, user: IUserDocument) => done(err, user));
    })
}