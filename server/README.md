# Server
Otvoriti folder ../server u Visual Studio Code

npm install

npm start


## Dostupne rute

Mogu se videti u index.ts i odgovarajucim fajlovima u ruteru

### Stories

- Dohvatanje svih:   GET  http://localhost:3000/stories/
- Postavljanje nove: POST http://localhost:3000/stories/

### User
- Auth with Google : GET http://localhost:3000/auth/google
- Google auth callback: GET http://localhost:3000/auth/google/callback
- Logout : GET http://localhost:3000/auth/logout

- Registration: POST http://localhost:3000/user/registration
