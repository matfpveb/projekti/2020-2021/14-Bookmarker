import mongoose, {Document, Model} from 'mongoose';
const bcrypt = require('bcrypt');
const jwtUtil = require('../middleware/jwt');

const SALT_ROUNDS = 10;

export type UserRole = "admin"|"user";

export interface UserSearchI{
    name?: string;
    username?: string;
    surname?: string;
}


export interface IUserDocument extends Document{
    googleId: string,
    name: string,
    surname: string
    username: string,
    password: string,
    role:string,
    email: string,
    image:string,
    createdAt: Date,
    age:Number,
    place:String,
    setPassword(password: string): void,
    isValidPassword(password: string): boolean,
    addFollowingId(idUser: string): void,
    following: any[],
    followers: any[],
    salt: any,
    hash: any,
    comparePassword(password: string): boolean,
}

export interface IUserModel extends Model<IUserDocument> {
    getUserById(userId: any) : Promise<IUserDocument>
    getUserByUsername(username: string): Promise<IUserDocument>
    getUserJWTByUsername(username: string): any
    registerNewUser(name: string, surname: string, email: string, username: string, password: string): any
    follow(idUser: string, idFollow: string): any
    unfollow(idUser: string, idFUnfollow: string): any
    updateUserData(username: string, name: string, surname: string, email: string, password: string, age: string, place: string): any
    changeUserProfileImage(username: string, imgUrl: string): any
}

const userSchema = new mongoose.Schema<IUserDocument>({
    googleId: {
        type: String,
        // required: true,
    },
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: mongoose.Schema.Types.String,
        // required: true,
      },
    salt: {
        type: mongoose.Schema.Types.String,
        // required: true,
      },    
    email: {
        type: String,
        required: true
    },
    following: {
        type: [String],
        default: []
    },
    followers: {
        type: [String],
        default: []
    },
    image:{
        type: String,
        default: 'uploads/default_avatar.png'
    },
    createdAt:{
        type: Date,
        default: Date.now
    },
    place:{
        type:String,
        default:""
    },
    age:{
        type:Number,
        default:0
    },
    role:{
        type: String,
        default: "user"
    }
});

userSchema.index({ username: 'text' });

userSchema.methods.setPassword = async function (password: string) {
    this.salt = await bcrypt.genSalt(SALT_ROUNDS);
    this.hash = await bcrypt.hash(password, this.salt);;
  };

  userSchema.methods.isValidPassword = async function (password: string) {
    return await bcrypt.compare(password, this.hash);
  };


userSchema.statics.getUserById = async function(userId: any) {
    const user = await this.findById(userId).exec();

    if (user) {
        return Promise.resolve(user);
    } else {
        return Promise.reject();
    }
};

userSchema.statics.getUserByUsername = async function(username: string) {
    const user = await this.findOne({ username }).exec();
    return user;
};

userSchema.statics.getUserJWTByUsername = async function(username: string) {
    const user = await User.getUserByUsername(username);
    if (!user) {
      throw new Error(`User with username ${username} does not exist!`);
    }
    return jwtUtil.generateJWT({
      id: user.id,
      username: user.username,
      email: user.email,
      name: user.name,
      surname: user.surname,
      image: user.image,
      following: user.following,
      followers: user.followers,
      createdAt: user.createdAt,
      age: user.age,
      place: user.place,
      role: user.role
    });
  };
  
userSchema.statics.registerNewUser = async function(name: string, surname: string, email: string, username: string, password: string) {

    const user = new User();

    user.name = name;
    user.surname = surname;
    user.email = email;
    user.username = username;
    await user.setPassword(password);

    await user.save();

    return User.getUserJWTByUsername(username);
};

userSchema.statics.updateUserData = async function(username: string, name: string, surname: string, email: string, password: string, age: string, place: string){

  const user = await User.getUserByUsername(username);
  user.name = name;
  user.surname = surname;
  user.email = email;
  user.age = parseInt(age);
  user.place = place;

  if(password !== '')
    await user.setPassword(password);
    
  await user.save();
  
  return User.getUserJWTByUsername(username);
};

userSchema.statics.changeUserProfileImage = async function(username: string, imgUrl: string) {
    const user = await User.getUserByUsername(username);
    user.image = imgUrl;
    await user.save();
}

userSchema.statics.follow = async function(idUser: string, idFollow: string){

    const user = await User.getUserById(idUser);
    user.following.push(idFollow);

    const user2 = await User.getUserById(idFollow);
    user2.followers.push(idUser);

    await user.save();
    await user2.save();
    
    return User.getUserJWTByUsername(user.username);
}

userSchema.statics.unfollow = async function(idUser: string, idUnfollow: string){

    const user = await User.getUserById(idUser);

    user.following.forEach((value,index)=>{
        if(value==idUnfollow) 
            user.following.splice(index,1);
    });

    const user2 = await User.getUserById(idUnfollow);

    user2.followers.forEach((value,index)=>{
        if(value==idUser) 
            user2.followers.splice(index,1);
    });

    await user.save();
    await user2.save();

    return User.getUserJWTByUsername(user.username);
}

userSchema.methods.comparePassword = async function(password: string) {
    let isValid = await bcrypt.compare(password, this.password) ? true : false;
    return isValid;
};

const User = mongoose.model<IUserDocument, IUserModel>('User', userSchema);

export async function searchUsers(options:UserSearchI) {
    let result : Array<object> = [];
    let criteriaOptions = {};
    if(options.name){
        criteriaOptions = {name: {$regex :options.name, $options: "i"}} 
    }
    if(options.surname){
        criteriaOptions = {...criteriaOptions, surname: {$regex : options.surname, $options :"i"}};
    }
    if(options.username){criteriaOptions = {...criteriaOptions, surname: {$regex : options.surname , $options:"i"}};
        criteriaOptions = {...criteriaOptions, username: {$regex : options.username, $options: "i"}}
    }
    (await User.find(criteriaOptions).exec()).forEach(user=>{
        result.push(user);
    });
    return result;
}

export async function simpleSearchUser(keywords: string){
    let result: Array<object> = [];
    const indexName = "u_ss_name_user_surname";
    let indexes: object = await User.collection.getIndexes();
    if(!Object.keys(indexes).includes(indexName)){
        await User.collection.createIndex({
            username: "text",
            surname: "text",
            name: "text"
        }, {
            name: indexName
        });
    }
    (await User.find({$text: {$search: keywords}})).forEach(user=>{
        result.push(user);
    });
    return result;
}

export default User;