import { Router, Request, Response } from "express";
import Author from '../models/author';
import { StatusCodes } from 'http-status-codes';
import multer from "multer";
import { ensureAdmin } from "../middleware/auth";
let multerFile: Express.Multer.File;

const addAuthorRouter = Router();

const storage = multer.diskStorage({
    destination: function (req: Request, file: Express.Multer.File, callback: (error: Error | null, destination: string) => void) {
     callback(null, './uploads/');
 },
 filename: function (req: Request, file: Express.Multer.File, callback: (error: Error | null, filename: string) => void) {
    callback(null, file.originalname);
 }
});
const upload = multer({storage: storage});


addAuthorRouter.get('/:id', async (req,res) => {
    try{
        const author = await Author.getAuthorById(req.params.id);
        res.status(StatusCodes.OK).json(author);
    }
    catch(err){
        console.error("[err while getting author by id]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

addAuthorRouter.get('/', async (req,res) => {
    try{
        const authors = await Author.find();
        res.status(StatusCodes.OK).json(authors);
    }
    catch(err){
        console.error("[err while geting all authors]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});
// TODO: ensureAdmin, 
addAuthorRouter.post('/', upload.single('image'), async(req, res, next) => {
    const name = req.body.name;
    const bio = req.body.bio;
    const imagePath = req.file.path;
    const nationality = req.body.nationality;
    const born = req.body.born;
    const died = req.body.died;
    

    try{
        let book = Author.addAuthor(name, bio, imagePath, nationality, born, died);
        res.status(StatusCodes.CREATED).send(book);
    }
    catch(err){
        console.error(err)
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

export default addAuthorRouter;