import { Router } from "express";
import { StatusCodes } from 'http-status-codes';
import Book from '../models/book';

const booksRouter = Router();

booksRouter.get('/', async (req,res) => {
    try{
        const book = await Book.getAllBooks();
        res.status(StatusCodes.OK).json(book);
    }
    catch(err){
        console.error("[err while geting book]", err);
        res.sendStatus(StatusCodes.NOT_FOUND);
    }
});

export default booksRouter;