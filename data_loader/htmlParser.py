import re
import requests
import json

siteOrigin = "https://www.goodreads.com"

listUrls = [
    # 'https://www.goodreads.com/list/show/73283.100_Mysteries_and_Thrillers_to_Read_in_a_Lifetime_Readers_Picks'
    "https://www.goodreads.com/list/show/264.Books_That_Everyone_Should_Read_At_Least_Once"]

def getPageContent(url: str):
    page = requests.get(url)
    return page.content.decode("utf-8")

# vraca tabelu sa listom knjiga 
def getTableBodyBookList(content: str)->str:
    books = re.search("<table .*? class=\"tableList .*?>(.*?)</table>", content, re.S|re.X)
    if books is not None:
        return books.group(0)
    return "Lista knjiga nije pronadjena"

# vrati sve knjige iz prosledjene tabele sa listom knjiga
def getBooks(bookTable: str) -> list:
    books=[]
    bookRE = re.compile("""
     <tr.*?itemtype=\"http://schema.org/Book\">
     .*? <a  .*?  class=\"bookTitle\" .*? href=\"(?P<bookUrl>.*?)\" .*? <span.*?>(?P<bookTitle>.*?)</span>
     .*? <a .*? class=\"authorName\" .*? href=\"(?P<authorUrl>.*?)\".*?<span.*?>(?P<authorName>.*?)</span>
     .*?
     </tr>
    """, re.S|re.X)
    print("Trazenje knjiga...")
    # results = bookRE.findall(bookTable)
    for match in bookRE.finditer(bookTable):
        info = {
            "bookTitle" : match.group("bookTitle"),
            "bookUrl": match.group("bookUrl"),
            "authorName" : match.group("authorName"),
            "authorUrl": match.group("authorUrl"),
        }
        books.append(info)
    return books

# vraca urlove
def getAuthors(bookTable: str)->list:
    authors=[]
    bookRE = re.compile("""
     <tr.*?itemtype=\"http://schema.org/Book\">
     .*? <a .*? class=\"authorName\" .*? href=\"(?P<authorUrl>.*?)\"
    """, re.S|re.X)
    print("Trazenje knjiga...")
    # results = bookRE.findall(bookTable)
    for match in bookRE.finditer(bookTable):
        authors.append(match.group("authorUrl"))
    return authors

def getBookLinks(bookTable: str) -> list:
    books=[]
    bookRE = re.compile("""
     <tr.*?itemtype=\"http://schema.org/Book\">
     .*? <a  .*?  class=\"bookTitle\" .*? href=\"(?P<bookUrl>.*?)\"
    """, re.S|re.X)
    print("Trazenje knjiga...")
    # results = bookRE.findall(bookTable)
    for match in bookRE.finditer(bookTable):
        books.append(match.group("bookUrl"))
    return books

# kada se otvori url knjige nalazi podatke za knjigu
def getBookInfo(content: str)->map:
    info = {}
    bookTitleRE = re.compile("id=\"bookTitle.*?>(?P<title>.*?)</h1", re.S|re.X)
    bookImageRE = re.compile("<img .*? id=\"coverImage\" .*? src=\"(?P<imageUrl>.*?)\"", re.S|re.X)
    bookDescriptionRE = re.compile("<span .*? id=\"freeText[0-9]+ .*? >(?P<desc>.*?)</span>", re.S|re.X)
    bookGenresRE = re.compile("class=\"left.*?<a.*?bookPageGenreLink.*?>(?P<genre>.*?)</a>", re.S|re.X)
    
    title = bookTitleRE.search(content)
    if title is not None:
        info['bookTitle'] = title.group('title')

    image = bookImageRE.search(content)
    if image is not None:
        info['bookImageUrl'] = image.group('imageUrl')

    bookDescription = bookDescriptionRE.search(content)
    if bookDescription is not None:
        info['bookDescription'] = bookDescription.group('desc')

    info['genres'] = []
    for genre in bookGenresRE.finditer(content):
        info['genres'].append(genre.group('genre'))

    info['reviews'] = getReviews(content)

    return info

def getReviews(content: str)->list:
    info = []
    bookReviewsRE = re.compile("""
        itemtype=\"http://schema.org/Review\" .*?
        <a .*? imgcol .*? href=\"(?P<userUrl>.*?)\" .*?
        (<span .*? staticStars .*? title=\"(?P<rate>.*?)\")? .*?
        class=\"reviewDate\ createdAt\ right\".*? >(?P<postedAt>.*?)</a> .*?
        itemprop=\"author\" .*? <a .*? name=\"(?P<userName>.*?)\" .*?
        <span.*? id=\"freeText[0-9]*\" .*?>(?P<userComment>.*?)</span>
    """, re.S|re.X )
    i= 0
    for review in bookReviewsRE.finditer(content):
        rate = ""
        try:
            rate = review.group("rate")
        except:
            print('nema rate')
            
        info.append({
            "postedAt": review.group("postedAt"),
            "userName": review.group("userName"),
            "userComment": review.group("userComment"),
            "rate": rate
        })
        i+=1
        if i > 2:
            break
    return info

def getAuthorInfo(content: str)->map:
    info = {}
    authorNameRE = re.compile("<h1 .*? class=\"authorName\" .*? <span.*?>(?P<authorName>.*?)</span>", re.S|re.X)
    authorBioRE = re.compile("<span .*? id=\"freeTextauthor[0-9]+\" .*?>(?P<authorBio>.*?)</span>", re.S|re.X)
    authorImageUrlRE = re.compile("authorLeftContainer\" .*?<img .*? src=\"(?P<authorImageUrl>.*?)\"", re.S|re.X)
    authorBornRE = re.compile("birthDate.*?(?P<authorBorn>[0-9]{4})", re.S|re.X)
    authorDiedRE = re.compile("deathDate.*?(?P<authorDied>[0-9]{4})", re.S|re.X)
    tmp = authorNameRE.search(content)

    if tmp is not None:
        info['authorName'] = tmp.group('authorName')
    tmp = authorBioRE.search(content)
    if tmp is not None:
        info['authorBio'] = tmp.group('authorBio')
    tmp = authorImageUrlRE.search(content)
    if tmp is not None:
        info['authorImageUrl'] = tmp.group('authorImageUrl')
    tmp = authorBornRE.search(content)
    if tmp is not None:
        info['authorBorn'] = tmp.group('authorBorn')
    tmp = authorDiedRE.search(content)
    if tmp is not None:
        info['authorDied'] = tmp.group('authorDied')

    return info


def getUrl(url: str)->str:
    if not url.startswith("http"):
        return siteOrigin+url
    return url

def saveToJson(filename: str, data: any):
    print("Cuvanje podataka u fajl '"+filename+".json'...")
    with open(filename+'.json', 'w+') as json_file:
        json.dump(data, json_file)
    print("Cuvanje podataka u fajl '"+filename+".json' gotovo.")

if __name__ == "__main__":
    json_data = []
    # preuzimanje liste knjiga # RADI
    print("Zapoceto preuzimanje podataka...")
    pageContent = getPageContent(listUrls[0])
    bookList = getTableBodyBookList(pageContent)
    authors = getAuthors(bookList)
    # books = getBooks(bookList) # OLD
    for author in authors[0:10]:
        print(author)
        author_data = {}
        content = getPageContent(author)
        info = getAuthorInfo(content)
        author_data['info'] = info
        books = getBookLinks(content)
        books_data = []
        for book in books:
            print(book)
            book_data = {}
            url = getUrl(book)
            bookPageContent = getPageContent(url)
            bInfo = getBookInfo(bookPageContent)
            bInfo['url'] = book
            book_data['bookInfo']=bInfo
            books_data.append(book_data)
        author_data['books'] = book_data
        json_data.append(author_data)
    saveToJson("authors", json_data)

    #test
    # print(getReviews(getPageContent("https://www.goodreads.com/book/show/656.War_and_Peace")))
    
    # OLD
    # preuzimanje podataka o pojedinacnim knjigama
    # print("Preuzimanje podataka o knjigama i autorima...")
    # test
    # bookPageContent = getPageContent("https://www.goodreads.com/book/show/33128934-stillhouse-lake")
    # print(getBookInfo(bookPageContent))
    # authorPageContent = getPageContent("https://www.goodreads.com/author/show/1077326.J_K_Rowling")
    # print(getAuthorInfo(authorPageContent))
    # authorPageContent = getPageContent("https://www.goodreads.com/author/show/1455.Ernest_Hemingway")
    # print(getAuthorInfo(authorPageContent))
    # for book in books:
    #     try:
    #         bookPageContent = getPageContent(getUrl(book['bookUrl']))
    #         tmp = getBookInfo(bookPageContent)
    #         book['bookImageUrl'] = tmp['bookImageUrl']
    #         book['bookDescription']=tmp['bookDescription']
    #         book['genres'] = tmp['genres']
    #         authorPageContent = getPageContent(getUrl(book['authorUrl']))
    #         tmp = getAuthorInfo(authorPageContent)
    #         book['authorName'] = tmp['authorName']
    #         book['authorBio']=tmp['authorBio']
    #         book['authorImageUrl'] = tmp['authorImageUrl']
    #         try:
    #             book['authorBorn'] = tmp['authorBorn']
    #         except:
    #             pass
    #         try:
    #             book['authorDied'] = tmp['authorDied']
    #         except:
    #             pass
    #     except:
    #         print('greska u obradi '+ getUrl(book['bookUrl'])+ " ili " + getUrl(book['authorUrl']))


    # print("Cuvanje podataka u fajl 'books.json'...")
    # with open('books.json', 'w+') as json_books_file:
    #     json.dump(books, json_books_file)

    

